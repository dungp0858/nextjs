"use client";
import React from 'react'
import { Sheet, SheetContent, SheetTrigger } from '@/components/ui/sheet';
import Link from "next/link";
import { CiMenuFries } from 'react-icons/ci';
import { usePathname } from 'next/navigation';

const links = [
    {
        name: "Trang chủ",
        path: "/"
    },
    {
        name: "Tóm tắt",
        path: "/tomtat"
    },
    {
        name: "Liên hệ",
        path: "/lienhe"
    },

]

const MenuMobile = () => {
    const pathName = usePathname();
    return (
        <Sheet>
            <SheetTrigger className='flex justify-center items-center'>
                <CiMenuFries className='text-[32px] text-accent'></CiMenuFries>
            </SheetTrigger>
            <SheetContent className='flex flex-col'>
                <div className='mt-32 mb-40 text-center text-2xl'>
                    <Link href="/">
                        <h1>Phạm Văn Dũng</h1> <span className='text-accent'>.</span>
                    </Link>
                </div>
                <nav>
                    {links.map((link, index) => {
                        return (
                            <Link href={link.path} key={index} className={`${link.path === pathName && "text-accent border-b-2 border-accent"} capitalize font-medium hover:text-accent transition-all`}>
                                {link.name}
                            </Link>
                        )
                    })}
                </nav>

            </SheetContent>
        </Sheet>
    )
}

export default MenuMobile
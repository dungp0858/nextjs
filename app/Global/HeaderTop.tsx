import React from 'react'
import Link from 'next/link';
import { Button } from '@/components/ui/button';
import Menu from './Menu';
import MenuMobile from './MenuMobile';

const HeaderTop = () => {
  return (
    <header className="py-8 xl:py-12 text-white">
        <div className="container mx-auto flex justify-between items-center"> 
          {/* Logo */}
          <Link href="/">
            <h1 className="text-3xl font-semibold">
              Phạm Văn Dũng <span  className="text-accent">.</span>
            </h1>
          </Link>
          {/* {menu} */}
          <div className="hidden xl:flex items-center gap-8">
            <Menu />
          </div>
          {/* menu mobile */}
          <div className="xl:hidden">
            <MenuMobile/>
          </div>
        </div>
    </header>
  )
}
export default HeaderTop;
